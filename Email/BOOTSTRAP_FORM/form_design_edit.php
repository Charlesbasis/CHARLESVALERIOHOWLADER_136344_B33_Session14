<!DOCTYPE html>
<html lang="en">
<head>
    <title>Email</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../../asset/bootstrap/css/bootstrap.min.css">
    <script src="../../asset/bootstrap/js/jquery.min.js"></script>
    <script src="../../asset/bootstrap/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>Edit Name & Email</h2>
    <form>
        <div class="form-group">
            <label for="Name">Name:</label>
            <input type="text" class="form-control" id="Name" placeholder="Enter Name">
        </div>
        <div class="form-group">
            <label for="Email">Email:</label>
            <input type="email" placeholder="Enter email">
        </div>

        <button type="save" class="btn-default">Save</button>
        <button type="Edit" class="btn-default">Edit</button>
        <button type="Delete" class="btn-danger">Delete</button>
    </form>
</div>

</body>
</html>


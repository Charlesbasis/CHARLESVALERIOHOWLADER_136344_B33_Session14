<!DOCTYPE html>
<html lang="en">
<head>
    <title>Edit BookTitle</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../../asset/bootstrap/css/bootstrap.min.css">
    <script src="../../asset/bootstrap/js/jquery.min.js"></script>
    <script src="../../asset/bootstrap/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>Edit Book Name & Author</h2>
    <form>
        <div class="form-group">
            <label for="Book Name">Book Name:</label>
            <input type="text" class="form-control" id="Book Name" placeholder="Edit Book Name">
        </div>
        <div class="form-group">
            <label for="Author">Author:</label>
            <input type="text" class="form-control" id="Author" placeholder="Edit Author">
        </div>
        <div class="checkbox">
            <label><input type="checkbox"> Remember me</label>
        </div>
        <button type="save" class="btn-default">Save</button>
        <button type="Edit" class="btn-default">Edit</button>
        <button type="Delete" class="btn-danger">Delete</button>
    </form>
</div>

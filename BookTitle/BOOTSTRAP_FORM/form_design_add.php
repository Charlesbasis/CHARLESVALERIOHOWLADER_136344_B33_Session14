<!DOCTYPE html>
<html lang="en">
<head>
    <title>Add BookTitle</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../../asset/bootstrap/css/bootstrap.min.css">
    <script src="../../asset/bootstrap/js/jquery.min.js"></script>
    <script src="../../asset/bootstrap/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>Add Book List with Author</h2>
    <form>
        <div class="form-group">
            <label for="Book Title">Book Title:</label>
            <input type="text" class="form-control" id="Book Title" placeholder="Enter Book Title">
        </div>
        <div class="form-group">
            <label for="Author">Author:</label>
            <input type="text" class="form-control" id="Author" placeholder="Enter Author">
        </div>

        <button type="save" class="btn-lg">Save</button>
    </form>
</div>

</body>
</html>


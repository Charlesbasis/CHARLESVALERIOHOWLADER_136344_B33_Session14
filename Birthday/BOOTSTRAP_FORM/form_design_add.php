<!DOCTYPE html>
<html lang="en">
<head>
    <title>Birthday</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../../asset/bootstrap/css/bootstrap.min.css">
    <script src="../../asset/bootstrap/js/jquery.min.js"></script>
    <script src="../../asset/bootstrap/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>Add Birthdate</h2>
    <form>
        <div class="form-group">
            <label for="Name">Name:</label>
            <input type="text" class="form-control" id="Name" placeholder="Enter Name">
        </div>
        <div class="form-group">
            <label for="Birthdate">Birthdate:</label>
            <input type="date" class="form-control" id="Birthdate" placeholder="Enter Birthdate">
        </div>

        <button type="save" class="btn-lg">Save</button>
    </form>
</div>

</body>
</html>


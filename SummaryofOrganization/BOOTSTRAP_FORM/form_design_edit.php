<!DOCTYPE html>
<html lang="en">
<head>
    <title>SummaryofOrganization</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../../asset/bootstrap/css/bootstrap.min.css">
    <script src="../../asset/bootstrap/js/jquery.min.js"></script>
    <script src="../../asset/bootstrap/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>Edit Organization Name & Summary</h2>
    <form>
        <div class="form-group">
            <label for="Organization Name">Organization Name:</label>
            <input type="text" class="form-control" id="Organization Name" placeholder="Enter Organization Name">
        </div>
        <div class="form-group">
            <label for="Organization Summary">Organization Summary:</label>
            <input type="text"  class="form-control" id="Organization Summary" placeholder="Enter Organization Summary">
        </div>

        <button type="save" class="btn-default">Save</button>
        <button type="Edit" class="btn-default">Edit</button>
        <button type="Delete" class="btn-danger">Delete</button>
    </form>
</div>

</body>
</html>


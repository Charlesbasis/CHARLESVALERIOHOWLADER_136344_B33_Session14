<!DOCTYPE html>
<html lang="en">
<head>
    <title>City</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../../asset/bootstrap/css/bootstrap.min.css">
    <script src="../../asset/bootstrap/js/jquery.min.js"></script>
    <script src="../../asset/bootstrap/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>Edit Name & City</h2>
    <form>
        <div class="form-group">
            <label for="Name">Name:</label>
            <input type="text" class="form-control" id="Name" placeholder="Edit Name">
        </div>
        <div class="form-group">
            <label for="City">City:</label>
            <select class="form-control" id="City">
                <option value="Enter your City">Select your City</option>
                <option value="Chittagong">Chittagong</option>
                <option value="Dhaka">Dhaka</option>
                <option value="Sylhet">Sylhet</option>
            </select>
        </div>

        <button type="save" class="btn-default">Save</button>
        <button type="Edit" class="btn-default">Edit</button>
        <button type="Delete" class="btn-danger">Delete</button>
    </form>
</div>
